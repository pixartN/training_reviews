<?php
namespace Training\Brands\Api;

interface AllnewsRepositoryInterface
{
	public function save(\Training\Brands\Api\Data\AllnewsInterface $news);

    public function getById($newsId);

    public function delete(\Training\Brands\Api\Data\AllnewsInterface $news);

    public function deleteById($newsId);
}
