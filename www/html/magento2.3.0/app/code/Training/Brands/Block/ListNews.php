<?php

namespace Training\Brands\Block;

Class ListNews extends \Magento\Framework\View\Element\Template
{
	protected $allNewsFactory;
	
	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		\Training\Brands\Model\AllnewsFactory $allNewsFactory
	){
		parent::__construct($context);
		$this->allNewsFactory = $allNewsFactory;
	}
	

	
	public function getListNews()
	{
		$page = ($this->getRequest()->getParam('p'))? $this->getRequest()->getParam('p') : 1;


		$collection = $this->allNewsFactory->create()->getCollection();


		$collection->setCurPage($page);

		return $collection;
	}

	protected function _prepareLayout(){
		parent::_prepareLayout();
		$this->pageConfig->getTitle()->set(__('Latest News'));

        return $this;
	}
	

}