<?php
namespace Training\Reviews\Model\ResourceModel;
class Helloworld extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Define main table
     */
    protected function _construct()
    {
        $this->_init('training_reviews', 'news_id');
    }
}