<?php

namespace Training\Reviews\Model;

use Magento\Framework\Model\AbstractModel;

class Helloworld extends AbstractModel
{
    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init('Training\Reviews\Model\ResourceModel\Helloworld');
    }
}