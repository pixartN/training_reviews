<?php
namespace Training\Reviews\Model\ResourceModel\Helloworld;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    protected function _construct()
    {
        $this->_init(
            'Training\Reviews\Model\Helloworld',
            'Training\Reviews\Model\ResourceModel\Helloworld'
        );
    }
}