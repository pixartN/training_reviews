<?php
namespace  Training\Reviews\Controller\Index;

use Magento\Framework\App\Action\Context;
use Training\Reviews\Model\Helloworld;


class Save extends \Magento\Framework\App\Action\Action

    {


    protected $_news_id;

    public function __construct( Context $context, Helloworld $news_id )


    {
        $this->_news_id = $news_id;

    parent::__construct($context);
}
    public function execute()
    {
        $news_id = $this->_news_id->create();
        $news_id->setData($data);
        if($news_id->save()){
            $this->messageManager->addSuccessMessage(__('You saved review'));
        }else{
            $this->messageManager->addErrorMessage(__('Review was not saved.'));
        }
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('reviews/index/index');
        return $resultRedirect;
    }
}